import docker
from flask import current_app

try:
    from flask import _app_ctx_stack as stack
except ImportError:
    from flask import _request_ctx_stack as stack

class DockerClient(object):

    def __init__(self,app=None):
        self.app = app
        if app is not None:
            self.init_app(app)

    def init_app(self,app):
        app.config.setdefault('DOCKER_HOST','unix:///var/run/docker.sock')
        if hasattr(app, 'teardown_appcontext'):
            app.teardown_appcontext(self.teardown)
        else:
            app.teardown_request(self.teardown)

    def connect(self):
        client = docker.Client(self.app.config.get('DOCKER_HOST'),
                               version='1.9', timeout=10)

        return client

    def teardown(self,exception):
        ctx = stack.top
        if hasattr(ctx, 'docker_client'):
            True


    @property
    def client(self):
        ctx = stack.top
        if ctx is not None:
            if not hasattr(ctx, 'docker_client'):
                ctx.docker_client = self.connect()
            return ctx.docker_client