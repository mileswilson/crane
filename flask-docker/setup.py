from setuptools import setup

setup(
    name='Flask-docker',
    version='1.0',
    license='BSD',
    author='Miles Wilson',
    author_email='miles@presentthinking.net',
    description='Provides a flask client to py-docker',
    py_modules=['flask_docker'],
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[
        'Flask',
        'docker-py'
    ],
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)