import unittest
import mock
from mock import MagicMock
import sys
import json
sys.path.append("../../../../")
import etcd
from flask import Flask
from prints.launch import b_launch


class TestLaunchFunctions(unittest.TestCase):
    
    def setUp(self):
        self.app = Flask(__name__)
        self.app.debug=True
        self.app.register_blueprint(b_launch.b_launch)
        self.client = self.app.test_client()
        self.app.getFreeHost = mock.Mock(return_value='host-b')
        
    def test_spare_hosts(self):
        self.app.etcd_client = MockEtcdClient(1)
        valid_hosts = self.client.get('/cluster/test').data
        host_array = json.loads(valid_hosts)
        self.assertTrue(type(host_array) is list) 
        self.assertTrue(len(host_array) > 0)

    def test_no_spare_hosts(self):
        self.app.etcd_client = MockEtcdClient(2)
        valid_hosts = self.client.get('/cluster/test').data
        host_array = json.loads(valid_hosts)
        self.assertTrue(type(host_array) is list)
        self.assertTrue(host_array[0] == 'host-b')  # Host-b mocked as the standard return for free host
        
        
class MockEtcdClient():
    def __init__(self,mode):
        self.mode = mode
        
    def read(self,readPath, recursive=False, sorted=False):
        if self.mode == 1:
            if readPath == '/apps/test/hosts':
                return [self.MockResponse('host-a', 'active')]
            if readPath == '/hosts':
                return [self.MockResponse('host-a', 'active'),
                        self.MockResponse('host-b', 'active'),
                        self.MockResponse('host-c', 'active')]

        if self.mode == 2:
            if readPath == '/apps/test/hosts':
                return [self.MockResponse('host-a', 'active'),
                        self.MockResponse('host-b', 'active')]
            if readPath == '/hosts':
                return [self.MockResponse('host-a', 'active'),
                        self.MockResponse('host-b', 'active')]

    class MockResponse(etcd.EtcdResult):
        
        def __init__(self,key,value):
            self.key = key
            self.value = value
        
        
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestLaunchFunctions)
    unittest.TextTestRunner(verbosity=2).run(suite)