import socket
from threading import Thread


def PickUnusedPort():
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.bind(('localhost', 0))
  addr, port = s.getsockname()
  s.close()
  return port

def decomposeTaggedPorts(portStrings):
    ports = {}
    for portString in portStrings:
        split_ports = portString.split(':')
        ports[split_ports[0]] = (split_ports[1],split_ports[2])
    return ports



def async(f):
    def wrapper(*args, **kwargs):
        thr = Thread(target = f, args = args, kwargs = kwargs)
        thr.start()
    return wrapper