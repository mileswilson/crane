#!/usr/bin/python
# -*- coding: utf-8 -*-
import docker
import serf
import socket
import sys
from flask import Flask, Response
import json
import sidekick
import re
import pprint
import etcd
import prints.launch
from flask_etcd import EtcdClient
from flask_docker import DockerClient
import logging
from logging.handlers import RotatingFileHandler

app = Flask(__name__)
gossipPort = 0
httpPort = 0
host_ip = 0
host_name = ''

sk = 0

etcd_client = ''
c = ''
_client = ''


def PickUnusedPort():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('localhost', 0))
    (addr, port) = s.getsockname()
    s.close()
    return port


def containerExist(name):
    containers = c.containers()
    print containers
    names = []
    for container in containers:
        for f_name in container['Names']:
            names.append(str(f_name))
    print str(names)
    print name
    return '/' + name in names


@app.route('/setup')
def setup():
    if not containerExist('serf-docker-host'):
        container_id = c.create_container('ctlc/serf', '',
                ports=[(7946, 'udp'), 7946, 7373],
                name='serf-docker-host')
        c.start(container_id, port_bindings={'7946/udp': gossipPort,
                7946: gossipPort, 7373: httpPort})

    return '127.0.0.1:' + str(httpPort)


@app.route('/setup-etcd/<discovery>')
def setupEtcd(discovery):
    etcd_4001 = app.config['etcd_4001']
    etcd_7001 = app.config['etcd_7001']
    print etcd_4001
    if not containerExist('etcd-host'):
        launchCommand = '-discovery https://discovery.etcd.io/{discovery} -n {hostname} -peer-addr={hostip}:{etcd_7001} -peer-bind-addr=0.0.0.0:7001 -bind-addr=0.0.0.0:4001 -addr={hostip}:{etcd_4001}'.format(discovery=discovery,
                hostname=host_name, hostip=host_ip,
                etcd_7001=etcd_7001, etcd_4001=etcd_4001)
        container_id = c.create_container('coreos/etcd', ports=[(7001,
                'udp'), 7001, 4001, (4001, 'udp')], name='etcd-host',
                command=launchCommand)
        c.start(container_id, port_bindings={
            '%s/udp' % 7001: etcd_7001,
            '%s' % 7001: etcd_7001,
            '%s/udp' % 4001: etcd_4001,
            '%s' % 4001: etcd_4001,
            })
    else:
        etcd_container = c.inspect_container('etcd-host')
        pprint.pprint(etcd_container)
        app.config['etcd_4001'] = etcd_container['NetworkSettings']['Ports']['4001/tcp'][0]['HostPort']
        app.config['etcd_7001'] = etcd_container['NetworkSettings']['Ports']['7001/tcp'][0]['HostPort']

    app.config['ETCD_HOST'] = '%s:%s' % (host_ip,app.config['etcd_4001'])
    print app.config['ETCD_HOST']
    app.etcd_client = EtcdClient(app).connect()
    app.etcd_client.write('/hosts/%s' % host_name, host_ip )
    print app.etcd_client.machines
    return 'etcd launched'


@app.route('/serfHost')
def serfHost():
    return '127.0.0.1' + str(gossipPort)


@app.route('/launchDummy')
def launchDummy():
    sk.launchContainer(
        'dummy',
        80,
        command='/launch.sh',
        role='web',
        vulcan_group='dummy',
        vulcan_port=80,
        )
    return 'Attempted Launch of Dummy App'


@app.route('/freeHost')
def getFreeHost():
    _response = _client.query(Name='load', Payload='').request()
    freeHosts = []
    for i in _response:
        if i.body != None:
            if i.body.has_key('Payload'):
                if i.body['Payload'] != None:
                    freeHosts.append((float(i.body['Payload']),
                            i.body['From']))
    freeHosts = sorted(freeHosts, key=lambda score: score[0],
                       reverse=True)
    return str(freeHosts[0])


@app.route('/launchlb')
def launchLb():
    print get_container_ports_as_tags('etcd-host')
    ports = json.loads(get_container_ports_as_tags('etcd-host'
                       ).get_data(as_text=True))
    etcd_ports = {}
    for port in ports:
        split_ports = port.split(':')
        etcd_ports[split_ports[0]] = split_ports[2]
    print etcd_ports
    sk.launchContainer(
        'mailgun/vulcand',
        80,
        'vulcan-lb',
        '/opt/vulcan/vulcand -apiInterface="0.0.0.0" -interface="0.0.0.0" -etcd="http://%s:%s" -port=80 -apiPort=8182'
             % (host_ip, etcd_ports['4001']),
        [8182],
        truePortMap=True,
        )


@app.route('/container/<path:container_id>/ports/tag')
def get_container_ports_as_tags(container_id):
    bindings = c.inspect_container(container_id)['HostConfig'
            ]['PortBindings']
    tags = []
    print container_id
    for bindingKey in bindings.keys():
        if not bindings[bindingKey] == None:
            hostPort = bindings[bindingKey][0]['HostPort']
            m = re.search('\d+', bindingKey)
            guestPort = m.group(0)
            formatted = '%s:%s:%s' % (guestPort, host_ip, hostPort)
            if formatted not in tags:
                tags.append(formatted)
    return Response(json.dumps(tags), mimetype='application/json')

if __name__ == '__main__':
    _client = serf.Client('127.0.0.1:7373', auto_reconnect=False)
    _client.connect()

    host_ip = sys.argv[1]
    app.config['HOST_IP'] = host_ip
    host_name = socket.gethostname()

    gossipPort = PickUnusedPort()
    httpPort = PickUnusedPort()
    app.config['etcd_7001'] = PickUnusedPort()
    app.config['etcd_4001'] = PickUnusedPort()
    app.config.from_pyfile('config.cfg')
    app.docker_client = DockerClient(app).connect()
    c = app.docker_client
    setupEtcd(app.config['ETCD_CONFIG_KEY'])
    app.register_blueprint(prints.launch.b_launch.b_launch, url_prefix='/launch')
    sk = sidekick.SideKickController(c, host_ip)
    handler = RotatingFileHandler('foo.log', maxBytes=10000, backupCount=1)
    handler.setLevel(logging.DEBUG)
    app.logger.addHandler(handler)
    app.run(debug=True, host='0.0.0.0')
