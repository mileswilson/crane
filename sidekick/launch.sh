#!/bin/bash
echo $HOST_IP
echo $1
env
serf agent -advertise $HOST_IP:$SERF_PORT -tag role=$SLAVE_ENV_ROLE >> /var/log/serf.log 2>&1 &


python /opt/sidekick.py $HOST_IP $1
