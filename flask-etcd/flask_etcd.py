import etcd
from flask import current_app

try:
    from flask import _app_ctx_stack as stack
except ImportError:
    from flask import _request_ctx_stack as stack

class EtcdClient(object):
    
    def __init__(self,app=None):
        self.app = app
        if app is not None:
            self.init_app(app)
    
    def init_app(self,app):
        app.config.setdefault('ETCD_HOST','127.0.0.1:4001')
        if hasattr(app, 'teardown_appcontext'):
            app.teardown_appcontext(self.teardown)
        else:
            app.teardown_request(self.teardown)
    
    def connect(self):
        split_config = self.app.config.get('ETCD_HOST').split(':')
        return etcd.Client(host=split_config[0], port=int(split_config[1]))
    
    def teardown(self,exception):
        ctx = stack.top
        if hasattr(ctx,'etcd_client'):
            #Do nothing - we cannot disconnect, weirdly.
            True
    @property
    def client(self):
        ctx = stack.top
        if ctx is not None:
            if not hasattr(ctx,'etcd_client'):
                ctx.etcd_client = self.connect()
            return ctx.etcd_client
