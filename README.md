# Crane

Crane is a basic cluster manager for docker, in it's current state you need to:

1. Vagrant up - this will install docker, serf and crane onto your vms.
1. Ssh onto both VMs, and build and install the python modules (flask-etcd and flask-docker)
1. docker pull coreos/etcd
1. docker pull mailgun/vulcand
1. Open the config.cfg file, and set a new cluster key from https://discovery.etcd.io/new
1. serf join <PAIR IP> (i.e. if you're running on 192.168.33.10, the pair is .11)
1. check with serf members
1. cd /vagrant/crane 
1. python crane.py <MACHINE IP ADDRESS> (e.g. 192.168.33.10 or 192.168.33.11)
